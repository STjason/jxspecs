#
# Be sure to run `pod lib lint JXServiceHelper.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
    s.name             = 'JXServiceHelper'
    s.version          = '0.0.18'
    s.summary          = 'A short description of JXServiceHelper.'
    s.description      = "starlux api service helper"
    #s.homepage         = 'http://gitlab.starlux-airlines.com/mobile/JXServiceHelper'
    s.homepage         = 'https://gitlab.com/STjason/jxspecs'
    # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
    s.license          = { :type => 'proprietary', :file => 'LICENSE' }
    s.author           = { 'Jaxun' => 'jaxunhuang@starlux-airlines.com' }
    #s.source           = { :git => 'http://gitlab.starlux-airlines.com/mobile/ios-common/JXServiceHelper.git', :tag => s.version }
    s.source           = { :git => 'git@gitlab.com:STjason/JXServiceHelper.git', :tag => s.version.to_s }
    #
    # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'
    
    s.ios.deployment_target = '10.0'
    s.swift_version = '4.0'
    
    s.source_files = 'JXServiceHelper/*.swift'
    s.frameworks   = 'UIKit', 'Foundation'
    
    # s.resource_bundles = {
    #   'JXServiceHelper' => ['JXServiceHelper/Assets/*.png']
    # }
    
    # s.public_header_files = 'Pod/Classes/**/*.h'
    # s.frameworks = 'UIKit', 'MapKit'
    s.dependency 'RxSwift', '~> 4.0'
    s.dependency 'RxCocoa', '~> 4.0'
    s.dependency 'RxAlamofire'
end
